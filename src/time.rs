pub fn human_readable(duration: chrono::Duration) -> String {
    format!("{}{}{}{}s",
        if duration.num_days() > 0 { format!("{}d", duration.num_days()) } else { "".to_owned() },
        if duration.num_hours() > 0 { format!("{}h", duration.num_hours() % 24) } else { "".to_owned() },
        if duration.num_minutes() > 0 { format!("{}m", duration.num_minutes() % 60) } else { "".to_owned() },
        duration.num_seconds() % 60)
}