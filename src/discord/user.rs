use serenity::framework::standard::Args;
use serenity::model::prelude::*;
use serenity::prelude::*;

pub enum MatchDiscordUser {
    Bot(User),
    Other(User),
    SameAsHost,
    InvalidForm(String),
    NoUserFound,
    NoArgument,
}

pub async fn match_discord_user(args: &Args, ctx : &Context, host_id: u64) -> MatchDiscordUser {
    match args.current() {
        Some(u) => {
            match serenity::utils::parse_username(u) {
                Some(other_id) => {
                    if other_id == host_id {        
                        return MatchDiscordUser::SameAsHost;
                    }
    
                    match UserId(other_id).to_user(&ctx).await {
                        Ok(u) => { 
                            if u.bot {
                                MatchDiscordUser::Bot(u.clone())
                            }
                            else {
                                MatchDiscordUser::Other(u.clone())
                            }
                        },
    
                        Err(_) => MatchDiscordUser::NoUserFound,
                    }
                }
                None => MatchDiscordUser::InvalidForm(u.to_owned())
            }
        },
        None => MatchDiscordUser::NoArgument,
    }
}