pub fn parse_channels(list: String) -> Vec<u64> {
    let split = list.trim().split(" ");

    let mut list = Vec::new();

    for channel in split {
        if let Some(c) = serenity::utils::parse_channel(channel) {
            list.push(c);
        }
    }

    list
}