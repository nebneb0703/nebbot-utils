#[macro_export]
macro_rules! auto_help {
    ($botname: ident, $colour:expr, $prefix:expr) => {
        use serenity::framework::standard::{
            CommandResult, HelpOptions, help_commands, help_commands::CustomisedHelpData,
            Args, CommandGroup,
            macros::help,
        };
        use serenity::prelude::*;
        use serenity::model::prelude::*;
        use serenity::utils::Colour;
        
        use std::collections::HashSet;
        #[help]
        #[command_not_found_text = "Could not find: `{}`."]
        #[max_levenshtein_distance(3)]
        #[indention_prefix = "+"]
        #[lacking_permissions = "Hide"]
        #[lacking_role = "Hide"]
        #[wrong_channel = "Nothing"] // todo: change this back
        pub async fn help(
            context: &Context,
            msg: &Message,
            args: Args,
            help_options: &'static HelpOptions,
            command_groups: &[&'static CommandGroup],
            owners: HashSet<UserId>
        ) -> CommandResult {
            let bot_prefix = ($prefix)(context.clone(), msg.clone()).await;

            match help_commands::create_customised_help_data(context, msg, &args, command_groups, &owners, help_options).await {
                CustomisedHelpData::NoCommandFound { .. } => {
                    msg.channel_id.send_message(&context.http, |m| m.embed(|e| e
                        .colour($colour)
                        .title(format!("{} Commands", stringify!($botname)))
                        .description(format!("**Command not found:** {}", args.current().unwrap()))
                    )).await?;
                },
        
                CustomisedHelpData::SuggestedCommands { suggestions, .. } => {
        
                    let mut commands = String::new();
        
                    for suggested_name in suggestions.0 {
                        commands.push_str(&format!("`{}`\n", suggested_name.name));
                    }
        
                    msg.channel_id.send_message(&context.http, |m| m.embed(|e| e
                        .colour($colour)
                        .title(format!("{} Commands", stringify!($botname)))
                        .description(format!("**Command not found:** {}\n\n Did you mean:\n\n{}", args.current().unwrap(), commands))
                    )).await?;
                },
        
                CustomisedHelpData::GroupedCommands { groups, .. } => {
                    msg.channel_id.send_message(&context.http, |m| m.embed(|e| {
                        e
                        .colour($colour)
                        .title(format!("{} Commands", stringify!($botname)))
                        .description(format!("Use `{}help <command-name>` for further details!", bot_prefix));

                        for pair in groups {
                            let group_prefix = if pair.prefixes.len() > 0 { 
                                pair.prefixes.join(" ") + " "
                            } else { "".to_owned() };

                            let mut commands = String::new();
    
                            let group = command_groups.iter().filter(|x| x.name == pair.name).next().unwrap();

                            if let Some(cmd) = group.options.default_command {
                                commands.push_str(&format!("`{}{}`\n", bot_prefix, group_prefix.trim()));
                            }

                            for cmd in pair.command_names {
                                commands.push_str(&format!("`{}{}{}`\n", bot_prefix, group_prefix, cmd.replace("`", "")));
                            }
    
                            e.field(pair.name, commands, false);
                        }
    
                        e
                    })).await?;
                },
        
                CustomisedHelpData::SingleCommand { command } => {
                    let group_prefix = if command.group_prefixes.len() > 0 { 
                        command.group_prefixes.join(" ") + " "
                    } else { "".to_owned() };

                    let mut aliases = String::new();
        
                    for a in &command.aliases {
                        aliases.push_str(&format!("`{}`\n", a.clone()));
                    }
        
                    msg.channel_id.send_message(&context.http, |m| m.embed(|e| {
                        e
                        .colour($colour)
                        .title(format!("{} Commands: {}", stringify!($botname), command.name))                    
                        .description(command.description.unwrap_or("<@200640533331181578> was too lazy to provide a description for this command, slap him pls"))
    
                        .field("Aliases", if aliases.is_empty() { "None" } else { &aliases }, false);
    
                        if let Some(c) = command.usage {
                            let group_prefix = if c.trim().is_empty() {
                                group_prefix.trim()
                            } else { &group_prefix };

                            e.field("Usage", format!("`{}{}{}`", bot_prefix, group_prefix, c.replace("`", "")), false);
                        }
    
                        e.field("Category", command.group_name, false)
    
                        .field("Availability", command.availability, false)
                    })).await?;
                },
        
                _ => {}
            };
        
            Ok(())
        }
    };
}