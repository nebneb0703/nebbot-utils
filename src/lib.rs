#![allow(dead_code)]
#![allow(unused_macros)]

#[macro_use]
extern crate diesel;

pub mod discord;

pub mod time;

pub mod types;

pub mod prelude;