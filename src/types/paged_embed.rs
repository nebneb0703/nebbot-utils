use std::sync::Arc;

use serenity::builder::{ CreateActionRow, CreateEmbed, CreateMessage, CreateComponents };

use serenity::framework::standard::CommandResult;

use serenity::model::prelude::*;
use serenity::prelude::*;

use futures::{ StreamExt, future::BoxFuture };

pub struct PagedEmbed {
    pages: Vec<(CreateEmbed, Option<CreateActionRow>)>,
    current_page: usize,
}

impl PagedEmbed {
    pub fn new() -> Self {
        Self {
            pages: Vec::new(),
            current_page: 0
        }
    }

    pub fn add_page<F, C>(&mut self, f: F, mut c: C) -> &mut Self
        where F: FnOnce(&mut CreateEmbed) -> &mut CreateEmbed,
        C: FnMut(&mut CreateActionRow) -> Option<&mut CreateActionRow>
    {   
        let mut embed = CreateEmbed::default();
        f(&mut embed);

        let mut action_row = CreateActionRow::default();

        let ar = match c(&mut action_row) {
            Some(_) => Some(action_row),
            None => None,
        };

        self.pages.push((embed, ar));

        self
    }

    pub fn page(&mut self, page: usize) {
        if page < self.pages.len() {
            panic!("PagedEmbed page out of bounds: attempted to set current page index to {} when the length is {}.", page, self.pages.len());
        }

        self.current_page = page;
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.pages.len()
    }

    fn create_embed(&self) -> (CreateEmbed, Option<CreateActionRow>) {
        let (mut embed, action_row) = self.pages[self.current_page].clone();
        embed.footer(|f| 
            f.text(format!("Page {} of {}", self.current_page + 1, self.pages.len()))
        );

        (embed, action_row)
    }

    fn create_components(&self, action_row: Option<CreateActionRow>) -> CreateComponents {
        let mut components = CreateComponents::default();

        if let Some(ar) = action_row {
            components.add_action_row(ar);
        }

        // ⬅️ ➡️
        if self.pages.len() > 1 {
            components.create_action_row(|ar| {
                if self.current_page > 0 {
                    ar.create_button(|b| b
                        .label("Previous")
                        .emoji(ReactionType::Unicode("⬅️".to_owned()))
                        .custom_id("__paged_embed_prev")
                        .style(ButtonStyle::Secondary)
                    );
                }

                if self.current_page + 1 < self.pages.len() {
                    ar.create_button(|b| b
                        .label("Next")
                        .emoji(ReactionType::Unicode("➡️".to_owned()))
                        .custom_id("__paged_embed_next")
                        .style(ButtonStyle::Secondary)
                    );
                }
                
                ar
            });
        }

        components
    }

    fn create_message<'a>(&self) -> CreateMessage<'a> {
        let mut message = CreateMessage::default();

        let (embed, action_row) = self.create_embed();

        message.set_embed(embed);

        let components = self.create_components(action_row);

        message.components(|x| {
            *x = components; x
        });

        message
    }

    pub async fn message<'a, C>(mut self, ctx: &Context, msg: &Message, mut c: C) -> CommandResult
        where C: FnMut(Arc<Interaction>) -> Option<BoxFuture<'a, CommandResult>>,
    {
        let mut create_message = self.create_message();

        let mut message = msg.channel_id.send_message(&ctx.http, |_| &mut create_message).await?;

        let mut collector = message.await_component_interactions(&ctx)
        .timeout(std::time::Duration::from_secs(60))
        .author_id(msg.author.id).await;

        while let Some(interaction) = collector.next().await {
            match &interaction.clone().data {
                Some(InteractionData::MessageComponent(MessageComponent { custom_id, ..})) => match &**custom_id {
                    "__paged_embed_prev" => {
                        if self.current_page > 0 {
                            self.current_page -= 1;

                            let (embed, action_row) = self.create_embed();
                            let components = self.create_components(action_row);

                            message.edit(&ctx.http, |e| e
                                .set_embed(embed)
                                .components(|x| {
                                    *x = components; x
                                })
                            ).await
                            .expect("PagedEmbed: Failed to edit original message.");

                            interaction.create_interaction_response(&ctx.http, |i| {
                                i.kind(InteractionResponseType::UpdateMessage)
                            }).await
                            .expect("PagedEmbed: Failed to respond to interaction.");
                        }
                    }

                    "__paged_embed_next" => {
                        if self.current_page + 1 < self.pages.len() {
                            self.current_page += 1;

                            let (embed, action_row) = self.create_embed();
                            let components = self.create_components(action_row);

                            message.edit(&ctx.http, |e| e
                                .set_embed(embed)
                                .components(|x| {
                                    *x = components; x
                                })
                            ).await
                            .expect("PagedEmbed: Failed to edit original message.");

                            interaction.create_interaction_response(&ctx.http, |i| {
                                i.kind(InteractionResponseType::UpdateMessage)
                            }).await
                            .expect("Failed to respond to interaction.");
                        }
                    }
                    _ => {}
                },
                None => continue,
                _ => {}
            }

            if let Some(f) = c(interaction) {
                f.await?;
            }
        }

        message.edit(&ctx.http, |m| m
            .components(|c| c.set_action_rows(vec![]))
        ).await?;

        Ok(())
    }
}