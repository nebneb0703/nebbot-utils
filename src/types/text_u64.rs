use diesel::prelude::*;
use diesel::sql_types::{ Text };
use diesel::serialize::{ ToSql, Output };
use diesel::deserialize::{ FromSql };
use diesel::backend::Backend;

use std::io::prelude::*;

#[derive(Debug, Copy, Clone, AsExpression, PartialEq, Eq, Hash)]
#[sql_type = "Text"]
pub struct TextU64(
    #[diesel(deserialize_as = "String")]
    pub u64
);

impl From<u64> for TextU64 {
    fn from(f: u64) -> Self {
        TextU64(f)
    }
}

impl<DB, ST> Queryable<ST, DB> for TextU64
where
    DB: Backend,
    String: Queryable<ST, DB>,
{
    type Row = <String as Queryable<ST, DB>>::Row;

    fn build(row: Self::Row) -> Self {
        TextU64(String::build(row).parse().expect("Could not convert String to u64"))
    }
}

impl<Db> ToSql<Text, Db> for TextU64
where 
    Db: Backend,
    String: ToSql<Text, Db>
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, Db>) -> diesel::serialize::Result {
        self.0.to_string().to_sql(out)
    }
}

impl<Db> FromSql<Text, Db> for TextU64
where 
    Db: Backend,
    String: FromSql<Text, Db>
{
    fn from_sql(bytes: Option<&Db::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(TextU64(String::from_sql(bytes)?.parse()?))
    }
}

#[derive(Debug, Clone, AsExpression, PartialEq, Eq, Hash)]
#[sql_type = "Text"]
pub struct TextU32(
    #[diesel(deserialize_as = "String")]
    pub u32
);

impl From<u32> for TextU32 {
    fn from(f: u32) -> Self {
        TextU32(f)
    }
}

impl<DB, ST> Queryable<ST, DB> for TextU32
where
    DB: Backend,
    String: Queryable<ST, DB>,
{
    type Row = <String as Queryable<ST, DB>>::Row;

    fn build(row: Self::Row) -> Self {
        TextU32(String::build(row).parse().expect("Could not convert String to u32"))
    }
}

impl<Db> ToSql<Text, Db> for TextU32
where 
    Db: Backend,
    String: ToSql<Text, Db>
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, Db>) -> diesel::serialize::Result {
        self.0.to_string().to_sql(out)
    }
}

impl<Db> FromSql<Text, Db> for TextU32
where 
    Db: Backend,
    String: FromSql<Text, Db>
{
    fn from_sql(bytes: Option<&Db::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(TextU32(String::from_sql(bytes)?.parse()?))
    }
}